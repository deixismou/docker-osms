# Open Source Media Server (OSMS)

This repository provides microservice configuration files for running a
companion media server to frontends like [OSMC](https://osmc.tv/). Unlike OSMC,
this is not a linux distribution itself, but a collection of microservices
intended to run on a RaspberryPi Docker host.

Many of these containers are borrowed from the
[LinuxServer.oi](https://fleet.linuxserver.io) project.

For a detailed example of how to provision a Raspberry Pi as a Docker host
machine, see the [project wiki](https://gitlab.com/deixismou/docker-osms/wikis/home).

## Configure the services

To configure the services edit `docker-compose.yml` to set your desired
filesystem mount-points and port numbers. The default configuration assumes that
your external drive or merger filesystem is mounted at `/media/storage`, and
expects the following directory tree to exists:

```
/media/storage/
├── nzbget
│   ├── complete
│   └── incomplete
├── transmission
│   ├── complete
│   ├── incomplete
│   └── watch
└── library 
    ├── movies
    ├── music
    └── tv
```

Then copy `global.env.example` to `global.env` and edit that file to configure
your environment variables (e.g. host ip, locale, default UID and GID).

Once you have configured `global.env`, you can build and launch the services
with `docker-compose`.

## Launch the Services

To launch the services for the first time, navigate to the top-level directory
of this repository (the same directory as `docker-compose.yml`) and bring the
services up with `docker-compose`. The first time you run this command, it will
take a fair while, since some of the containers need to be built locally.

```bash
# build and run the containers (you might need root privs)
$ docker-compose up -d
```

To update a running container when a new version is availiable, you can use the
following one-liner.

```bash
# update a running container with compose
$ docker-compose up -d --no-deps --build <service_name>
```

## Services Overview

### PVR Clients:
- [Sonarr](https://hub.docker.com/r/linuxserver/sonarr): TV PVR Client
- [Radarr](https://hub.docker.com/r/linuxserver/radarr): Movie PVR Client
- [Lidarr](https://hub.docker.com/r/linuxserver/headphones): Music PVR Client

### Download Clients:
- [NZBget](https://hub.docker.com/r/linuxserver/nzbdget): Usenet download client (primary downloader)
- [Transmission](https://hub.docker.com/r/linuxserver/transmission): Torrent download client (secondary downloader)

### Client Control Frontend:
- [Organizr](https://hub.docker.com/r/linuxserver/organizr): Unified web GUI for PVR and download clients

### Index Mirror / Proxy
- [hydra2](https://hub.docker.com/r/linuxserver/hydra2)
- [Jackett](https://hub.docker.com/r/linuxserver/jackett): Indexer proxy client

### Network Library Access
- [PureFTP](https://github.com/stilliard/docker-pure-ftpd): A robust, yet simple ftp server


```mermaid
graph LR;

kod(Client: KODI)

subgraph host[Host: Raspberry Pi]
    vnzb[volume: nzbget_config]
    vtra[volume: transmission_config]
    vlid[volume: lidarr_config]
    vrad[volume: radarr_config]
    vson[volume: sonarr_config]
    vjac[volume: jackett_config]
    vhyd[volume: hydra2_config]

    subgraph osms[Service: OSMS]
        nzb[image: nzbget]
        tra[image: transmission]
        lid[image: lidarr]
        rad[image: radarr]
        son[image: sonarr]
        jac[image: jackett]
        hyd[image: hydra2]
        ftp[image: pure-ftp]
    end

    subgraph fuse[Fuse Mount: mergerfs]
        store[volume: /media/storage]

        subgraph pool[External Disk Pool]
            med01[disk: /media/pool/disk_01]
            med02[disk: /media/pool/disk_02]
            med03[disk: /media/pool/disk_03]
            med04[disk: /media/pool/disk_04]
            medn[disk: /media/pool/disk_n]
        end
    
    end
end

kod --- ftp

vnzb -.- nzb
vtra -.- tra
vlid -.- lid
vrad -.- rad
vson -.- son
vjac -.- jac
vhyd -.- hyd

nzb -. zbdget/complete\nzbget/incomplete .- store
tra -. transmission/complete\ntransmission/incomplete .- store
lid -. music\nnzbget/complete/music\ntransmission/complete/music .- store
rad -. movies\nnzbget/complete/movies\ntransmission/complete/movies .- store
son -. tv\n/complete/tv\ntransmission/complete/tv .- store
ftp -. music\nmovies\ntv .- store

store --- med01
store --- med02
store --- med03
store --- med04
store --- medn

style host fill:#E2DADB
style osms fill:#A49694
style fuse fill:#4F5D75
style pool fill:#DCD6F7
style nzb fill:#FFDD4A
style tra fill:#FFDD4A
style lid fill:#FF8811
style rad fill:#FF8811
style son fill:#FF8811
style jac fill:#3C6997
style hyd fill:#3C6997
style ftp fill:#5ADBFF
```
